FROM golang:alpine AS builder
ARG VERSION
WORKDIR /build
COPY . .
RUN go get -d -v
RUN [ ! -z "${VERSION}" ]
RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build \
  -ldflags "-X main.version=${VERSION} -s -w -extldflags '-static'" -o ./hello-world

FROM scratch
EXPOSE 8080
COPY --from=builder /build/hello-world /hello-world
COPY static /static
ENTRYPOINT ["/hello-world"]

package main

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"os"
)

var (
	version = "dev"
	message = "Hell sQills"
)

type (
	Info struct {
		Hostname string `json:"hostname"`
		Version  string `json:"version"`
		Message  string `json:"message"`
	}
)

func main() {
	log.Printf("Starting hello-world application version %s", version)

	hostname, _ := os.Hostname()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl, _ := template.ParseFiles("static/index.html")
		data := struct {
			Title string
		}{
			Title: hostname,
		}
		tmpl.Execute(w, data)
	})

	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	})

	http.HandleFunc("/info", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		resp := Info{
			Hostname: hostname,
			Version:  version,
			Message:  message,
		}
		jsonResp, _ := json.Marshal(resp)
		w.Write(jsonResp)
	})

	s := http.Server{Addr: ":8080"}
	log.Fatal(s.ListenAndServe())
}
